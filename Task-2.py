#! /usr/bin/env python

import lhapdf
import sys

pdf_set = lhapdf.getPDFSet("NNPDF40_nnlo_as_01180")
for x in range(pdf_set.size):
    member = pdf_set.mkPDF(x)
    gluon_test_value = member.xfxQ(21, float(sys.argv[1]), 1e4)
    if(1e-20 < abs(gluon_test_value)):
        print("\nAnomaly Found\n")
        sys.exit(42)
    else:
        print("Absolute value: ", abs(gluon_test_value))

print("\n", pdf_set.name, "Stable at x = 1 for parton ID=21 (gluon)\n")

