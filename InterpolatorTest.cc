#include "LHAPDF/GridPDF.h"
#include <LHAPDF/Interpolator.h>
#include <iostream>

using namespace std;


int main(int argc, char* argv[]) {

  if (argc < 2) {
    cout << "Usage: testgrid <PDFNAME=NNPDF40_nnlo_as_01180>" << endl;
  }
  const string setname = (argc < 2) ? "NNPDF40_nnlo_as_01180" : argv[1];

  LHAPDF::PDF* basepdf = LHAPDF::mkPDF(setname);
  LHAPDF::GridPDF& pdf = * dynamic_cast<LHAPDF::GridPDF*>(basepdf);

  for (int pid : pdf.flavors()) {
    cout << pid << " = " << pdf.xfxQ(pid, 0.2, 124) << endl;
  }
  
  LHAPDF::Interpolator* interp = LHAPDF::mkInterpolator("cubic");
  
  pdf.setInterpolator(interp);
  
  cout<<"---------------------------"<<endl;

  for (int pid : pdf.flavors()) {
    cout << pid << " = " << pdf.xfxQ(pid, 0.2, 124) << endl;
  }
  
  cout << endl;
  return 0;
}
